<!DOCTYPE html>
<head>
  <title>Secret Chat</title>
  <link rel="stylesheet" href="styles.css">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="https://js.pusher.com/6.0/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    // Pusher.logToConsole = true;

    var pusher = new Pusher('78d6f475edcae6cf53ba', {
      cluster: 'ap2'
    });

    var channel_name = '<?php echo $_POST['channel'] ?>';
    var channel = pusher.subscribe(channel_name);
    channel.bind('send-message', function(data) {
      var handle = $('<strong>').append(document.createTextNode(data['name'] + ': '));
      var message = document.createTextNode(data['message']);
      $('#output').append($('<p>').append(handle, message));
      // $('#output').html($('#output').html() + '<p><strong>' + name + ': </strong>' + message + '</p>');
    });
  </script>
</head>
<body>
  <div id="mario-chat">
      <h2>Secret Chat</h2>
      <div id="chat-window">
          <div id="output"></div>
      </div>
      <input id="handle" type="text" placeholder="Username" />
      <input id="message" type="text" placeholder="Message" />
      <button id="send">Send</button>

  </div><br><br><br>
  <center><div id="details">No details of your chat are ever stored on any server. All communication is deleted the moment you close this tab. Find the source <a href="https://bitbucket.org/sir_unknown/secret-chat/" target="_blank">here</a>.</div></center>

  <script type="text/javascript">

    function send_text() {
      var handle = $('#handle').val();
      var message = $('#message').val();
      $.post('message.php', {
        channel: channel_name,
        name: handle,
        message: message
      });
      $('#message').val('');
    }

    $('#message').keypress(function(event) {
      var key = (event.keyCode ? event.keyCode : event.which);
      if (key == '13') {
        send_text();
      }
    });

    $('#send').click(function() {
      send_text();
    });
  </script>
</body>
